<?php

/**
 * @file
 */

/**
 * Implements hook_menu().
 */
function expire_admin_menu() {
  return array(
    'expire_admin' => array(
      'title' => 'Expire this page',
      'page callback' => 'expire_admin_expire_this_page',
      'access arguments' => array('expire page'),
      'type' => MENU_CALLBACK,
    ),
    'expire_admin_health_check' => array(
      'page callback' => 'expire_admin_health_check',
      'access callback' => 'user_access',
      'access arguments' => array('access expire health check'),
      'type' => MENU_CALLBACK,
    ),
    'admin/config/system/expire/expire-admin' => array(
      'title' => 'Expire Admin Settings',
      'description' => 'Configure Expire Admin Settings.',
      'page callback' => 'drupal_get_form',
      'type' => MENU_LOCAL_TASK,
      'page arguments' => array('expire_admin_configure_settings_form'),
      'access arguments' => array('administer site configuration'),
    ),
  );
}

/**
 * Implements hook_permission().
 */
function expire_admin_permission() {
  return array(
    'expire page' => array(
      'title' => t('Expire page'),
      'description' => t('Expire page.'),
    ),
    'access expire health check' => array(
      'title' => t('Access the Expire Admin health check page'),
      'description' => t('This page url is at <b>@url</b>.', array(
        '@url' => base_path() . 'expire_admin_health_check',
      )),
    ),
  );
}

/**
 * Implements hook_block_info().
 */
function expire_admin_block_info() {
  $blocks['expire'] = array(
    'info' => t('Expire page'),
    'cache' => DRUPAL_NO_CACHE,
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function expire_admin_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'expire':
      if (user_access('expire page')) {
        $block['subject'] = '';
        $path = url(drupal_is_front_page() ? '' : $_GET['q'], array('absolute' => TRUE));
        $block['content'] = '<span class="button">' . l(
          t('Expire this page'),
          'expire_admin',
          array(
            'attributes'  => array('class' => array('expire-page')),
            'query'       => array(
              'page' => $path,
              'token' => drupal_get_token('expire_admin_url'),
            ),
          )
        ) . '</span>';
      }
      break;
  }

  return $block;
}

/**
 * Implements hook_preprocess_menu_local_tasks().
 *
 * Add the "Expire Page" tab wherever applicable.
 */
function expire_admin_preprocess_menu_local_tasks(&$vars) {
  if (!variable_get('expire_admin_use_local_tasks', 0)) {
    return;
  }

  // Sometimes local tasks can be rendered more than once.
  // We only want to add this once per page.
  $done = &drupal_static(__FUNCTION__);
  if (!$done) {
    $done = TRUE;
    $current_path = current_path();
    if (user_access('expire page') && !path_is_admin($current_path)) {
      if (drupal_is_front_page()) {
        $current_path = '';
      }
      if (empty($vars['primary'])) {
        $vars['primary'] = array();
      }
      $vars['primary'][] = array(
        '#theme' => 'menu_local_task',
        '#link' => array(
          'href' => 'expire_admin',
          'title' => t('Expire Page'),
          'localized_options' => array(
            'query' => array(
              'page' => $current_path,
              'token' => drupal_get_token('expire_admin_url'),
            )
          ),
        ),
      );
    }
  }
}

/**
 * Menu callback.
 *
 * Checks for $_GET['page'] and expires that page.
 */
function expire_admin_expire_this_page() {
  if (isset($_GET['page'])) {
    if (isset($_GET['token']) && drupal_valid_token($_GET['token'], 'expire_admin_url')) {
      $url = $_GET['page'];
      ExpireAPI::executeExpiration(array($url));
      drupal_set_message(t('Expired cache for page @page', array('@page' => url($url, array('absolute' => TRUE)))));
      drupal_goto($_GET['page'], array('external' => FALSE));
    }
    else {
      drupal_set_message(t('Unable to expire page cache, missing or invalid token'), 'error');
    }
  }
  else {
    drupal_set_message(t('Unable to expire page cache, missing url'), 'error');
  }
  drupal_goto('<front>');
}

/**
 * Menu callback for Expire Page settings form.
 *
 * Allows switching of menu local task link.
 */
function expire_admin_configure_settings_form() {
  $form = array();

  $form['expire_admin_use_local_tasks'] = array(
    '#type' => 'checkbox',
    '#title' => t('Provide a local task tab.'),
    '#default_value' => variable_get('expire_admin_use_local_tasks', 0),
    '#description' => t('Add a menu local task to all pages for expire page.'),
  );

  $form['expire_admin_expire_urls'] = array(
    '#type' => 'textarea',
    '#title' => t('Expire custom URLs'),
    '#default_value' => '',
    '#rows' => 10,
    '#placeholder' => <<<EOL
Example URL patterns:

|wildcard - this will expire every url on the site.
services - this will expire the "/services" url path.
services|wildcard - this will expire all url paths like "/services", "/services/sub-path", but not "/services-extra".
EOL
,
    '#description' => t('List of custom URLs to expire on form submission.') . '<br/>' .
      t('Enter one relative URL per line. It can be the path of a node (e.g. !example1) or of any alias (e.g. !example2). However, it has to be the final URL, not a redirect (use the !link1 and !link2 modules).', array('!example1' => '<strong>node/1234</strong>', '!example2' => '<strong>my/path</strong>', '!link1' => l('Global Redirect', 'https://drupal.org/project/globalredirect'), '!link2' => l('Redirect', 'https://drupal.org/project/redirect'))) . '<br/>' .
      t('If you want to match a path with any ending, add "|wildcard" to the end of the line (see !link1 for details). Example: !example1 will match !example1a, but also !example1b, !example1c, etc.', array('!link1' => l('function cache_clear_all', 'https://api.drupal.org/api/drupal/includes%21cache.inc/function/cache_clear_all/7'), '!example1' => '<strong>my/path|wildcard</strong>', '!example1a' => '<strong>my/path</strong>', '!example1b' => '<strong>my/path/one</strong>', '!example1c' => '<strong>my/path/two</strong>')) . '<br/>'
  );

  // Set a submit handler manually because the default submit handler gets
  // overridden by the system_settings_form() submit handler.
  $form['#submit'][] = 'expire_admin_urls_submit';

  return system_settings_form($form);
}

/**
 * Custom submit handler for expiring URLs.
 */
function expire_admin_urls_submit($form, &$form_state) {
  if ($form_state['values']['expire_admin_expire_urls']) {
    $expire_urls = explode(PHP_EOL, $form_state['values']['expire_admin_expire_urls']);

    $urls = array();
    $sanitized_urls = array();

    // Remove any extra \r characters left behind.
    foreach ($expire_urls as $expire_url) {
      if ($expire_url = trim($expire_url)) {
        $urls[] = $expire_url;
        $sanitized_urls[] = check_plain($expire_url);
      }
    }

    ExpireAPI::executeExpiration($urls);
    drupal_set_message(t('Expired cache for provided URLs: !urls', array(
      '!urls' => '<br/>' . implode('<br/>', $sanitized_urls),
    )));
  }

  // Avoid storing the variable in system_settings_form() submit handler.
  $form_state['values']['expire_admin_expire_urls'] = NULL;
}

/**
 * Simple callback for the site's health check.
 *
 * Useful when working with varnish.
 */
function expire_admin_health_check() {
  // Disable caching.
  drupal_page_is_cacheable(FALSE);
  // This route should only be accessible if Drupal is running fine.
  drupal_add_http_header('X-Drupal-Status', 'OK');
  echo date(DATE_COOKIE, REQUEST_TIME) . ': ' . 'Drupal is running';
  drupal_exit();
}
